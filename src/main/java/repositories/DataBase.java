package repositories;

import java.util.ArrayList;
import java.util.List;
import domain.*;

public class DataBase {
	

	private List <User> user;
	private List <EnumerationValue> enumerationValue;
	
	List<User> users = new ArrayList<User>();
	List<UserRoles> roles = new ArrayList<UserRoles>();

	
	public List<User> getUser() {
		return user;
	}
	public void setUser(List<User> user) {
		this.user = user;
	}
	public List<EnumerationValue> getEnumerationValue() {
		return enumerationValue;
	}
	public void setEnumerationValue(List<EnumerationValue> enumerationValue) {
		this.enumerationValue = enumerationValue;
	}
}
