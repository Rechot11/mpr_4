package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.Entity;
import domain.EntityState;
import domain.User;
import unityofwork.UnitOfWorkRepository;

public class HsqlUsersRepository implements UserRepository, UnitOfWorkRepository {
	
	private DataBase database;		
	
	public HsqlUsersRepository () {
		this.database = database;
		}
	
	public HsqlUsersRepository (DataBase database) {
	this.database = database;
	}

	public User withId(int Id) {
		for (User user : database.getUser()) { 
			if (user.getId() == Id)
				return user; 
		}
		return null;
	}

	public List <User> allOnPage(PagingInfo page) {
		List <User> user = new ArrayList <User> ();

        for(User u : database.getUser())
            if(user.size() < page.getPageSize())
                user.add(u);

        return user;
	}

	public void add(Object entity) {
		this.database.users.add((User) entity);
	}

	public void delete(Object entity) {
		this.database.users.remove((User) entity);
		}

	public void modify(Object entity) {
		int index = database.getUser().indexOf(entity);
		if (index >= 0) { 
			database.getUser().get(index).setState(EntityState.Changed); 
		}	
	}

	public int count() {
		return database.getUser().size(); 
	}

	public User withLogin(String login) { 
		for (User user : database.getUser()) { 
			if (user.getLogin() == login)
				return user;
		}
		return null;
	}

	public User withLoginAndPassword(String login, String password) { 
		for (User user : database.getUser()) {
			if ((user.getLogin() == login) && (user.getPassword() == password)) 
				return user;
		}
		return null;
	}

	public void setupPermissions(User user) {
		for (User u : database.getUser()) {
			if (u.equals(user)) {
				u.setRolesPermissions(new ArrayList());
				u.setState(EntityState.Changed);
			}
		}
	}

	public void persistAdd(Entity entity) {		
		this.database.users.add((User)entity);
	}

	public void persistDelete(Entity entity) {
		this.delete(entity);
	}

	public void persistUpdate(Entity entity) {
		this.modify(entity);
		}
	
	
	}


